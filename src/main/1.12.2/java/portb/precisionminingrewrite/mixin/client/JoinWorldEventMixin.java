package portb.precisionminingrewrite.mixin.client;

import manifold.rt.api.NoBootstrap;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.play.server.SPacketJoinGame;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import portb.precisionminingrewrite.BehaviourUtil;

@NoBootstrap
@Mixin(NetHandlerPlayClient.class)
public class JoinWorldEventMixin
{
    @Inject(
            method = "handleJoinGame",
            at = @At("TAIL")
    )
    private void onJoinWorldEvent(SPacketJoinGame packetIn, CallbackInfo ci)
    {
        BehaviourUtil.loadToolConfigurations();
    }
}
