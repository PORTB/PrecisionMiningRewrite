package portb.precisionminingrewrite.mixin.client;

import manifold.rt.api.NoBootstrap;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.network.NetHandlerPlayClient;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@NoBootstrap
@Mixin(NetHandlerPlayClient.class)
public interface NetHandlerPlayClientAccessor
{
    @Accessor("clientWorldController")
    WorldClient accessClientWorldController();
}
