/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.precisionminingrewrite;

import manifold.ext.rt.api.auto;
import manifold.rt.api.NoBootstrap;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.fml.common.Mod;



#if MC12
    import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
    import net.minecraftforge.fml.common.eventhandler.EventPriority;
    import net.minecraftforge.fml.common.gameevent.TickEvent;
    import net.minecraft.init.SoundEvents;
    import net.minecraft.util.IProgressUpdate;
    import net.minecraftforge.fml.client.registry.ClientRegistry;
#else
    import net.minecraftforge.eventbus.api.EventPriority;
    import net.minecraftforge.eventbus.api.SubscribeEvent;
    import net.minecraftforge.fml.ModLoadingContext;
    import net.minecraftforge.event.TickEvent;
    import net.minecraftforge.client.event.ClientPlayerNetworkEvent;
#endif

#if !(MC16 || MC12)
    import net.minecraft.world.item.ItemStack;
    import net.minecraft.sounds.SoundEvents;
    import net.minecraftforge.fml.IExtensionPoint;
#endif

#if MC16
    import org.apache.commons.lang3.tuple.Pair;
    import net.minecraft.util.SoundEvents;
    import net.minecraftforge.fml.ExtensionPoint;
    import net.minecraftforge.fml.client.registry.ClientRegistry;
    import net.minecraft.util.Util;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////// DO NOT REMOVE THIS IMPORT (import java.util.Map) OR ELSE MANIFOLD WILL CAUSE A FUCKING WEIRD COMPILATION ERROR //////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //@formatter:off
    import java.util.Map;
    //@formatter:on
#elif MC17
    import net.minecraftforge.fmlclient.registry.ClientRegistry;
#elif MC18
    import net.minecraftforge.client.ClientRegistry;
#elif MC19_2 || MC19_3
    import net.minecraftforge.client.event.RegisterKeyMappingsEvent;
    import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
#endif

#if MC12 || MC16
    import net.minecraft.item.ItemStack;
#elif MC17 || MC18
    import net.minecraft.Util;
#endif

import static portb.precisionminingrewrite.Constants.heldItem;
import static portb.precisionminingrewrite.Constants.mc;

@NoBootstrap
#if MC12
@Mod(
        modid = "precisionmining",
        version = "5.0",
        clientSideOnly = true,
        acceptableRemoteVersions = "*",
        acceptableSaveVersions = "*",
        name = "precisionmining"
)
@net.minecraftforge.fml.relauncher.SideOnly(net.minecraftforge.fml.relauncher.Side.CLIENT)
#else
@Mod("precisionmining")
@net.minecraftforge.api.distmarker.OnlyIn(net.minecraftforge.api.distmarker.Dist.CLIENT)
#endif
public class PrecisionMining
{
    public static boolean hasPlayerBrokenBlock = false;
    
    public PrecisionMining()
    {
        //client side only
        #if MC16
            ModLoadingContext.get().registerExtensionPoint(ExtensionPoint.DISPLAYTEST, () -> Pair.of(() -> net.minecraftforge.fml.network.FMLNetworkConstants.IGNORESERVERONLY, (s, v) -> true));
        #elif MC17
            ModLoadingContext.get().registerExtensionPoint(IExtensionPoint.DisplayTest.class, () -> new IExtensionPoint.DisplayTest(() -> net.minecraftforge.fmllegacy.network.FMLNetworkConstants.IGNORESERVERONLY, (a, b) -> true));
        #elif !MC12
            ModLoadingContext.get().registerExtensionPoint(IExtensionPoint.DisplayTest.class, () -> new IExtensionPoint.DisplayTest(() -> net.minecraftforge.network.NetworkConstants.IGNORESERVERONLY, (a, b) -> true));
        #endif
        
        #if !(MC19_2 || MC19_3)
        ClientRegistry.registerKeyBinding(Constants.TOGGLE_PRECISION_MODE_KEYBIND);
        #else
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::registerKeyBindings);
        #endif
        
        MinecraftForge.EVENT_BUS.register(this);
    }

    #if (MC19_2 || MC19_3)
    @SubscribeEvent
    public void registerKeyBindings(RegisterKeyMappingsEvent event)
    {
        event.register(Constants.TOGGLE_PRECISION_MODE_KEYBIND);
    }
    #endif
    
    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void onRenderTooltip(ItemTooltipEvent event)
    {
        #if MC12
        if(mc().world == null || (mc().currentScreen instanceof IProgressUpdate))
            return;
        #endif
        
        if(BehaviourUtil.isItemTool(event.getItemStack()))
        {
            auto enabled = BehaviourUtil.isPrecisionModeEnabled(event.getItemStack());
            
            event.getToolTip()
                 .add(
                         //tt=tooltip
                         Constants.translatableComponent("prsm.tt." + enabled)
                            #if MC12 .getFormattedText() #endif
                 );
        }
    }

    #if !MC12
    @SubscribeEvent
    public void onJoinWorld(#if MC19_2 || MC19_3 ClientPlayerNetworkEvent.LoggingIn #else ClientPlayerNetworkEvent.LoggedInEvent #endif event)
    {
        BehaviourUtil.loadToolConfigurations();
    }
    #endif
    
    @SubscribeEvent
    public void clientTickEvent(TickEvent.ClientTickEvent event)
    {
        processMouseInput();
        handleKeybindings();
    }

    private void handleKeybindings()
    {
        //the player can only mine when they don't have a screen open, so only toggle if the screen is null
        if(
                #if MC12
                    Constants.TOGGLE_PRECISION_MODE_KEYBIND.isPressed() && mc().currentScreen == null
                #else
                    mc().screen == null && Constants.TOGGLE_PRECISION_MODE_KEYBIND.consumeClick()
                #endif
        )
        {
            auto heldItem = heldItem();

            if(heldItem != ItemStack.EMPTY && BehaviourUtil.isItemTool(heldItem))
                toggleMiningMode();
        }
    }

    private void toggleMiningMode()
    {
        auto heldItem = heldItem();

        if(!heldItem.isEmpty() && BehaviourUtil.isItemTool(heldItem) /*&& BehaviourUtil.isToolConfigurationLoaded*/)
        {
            //this is a client-side only mod, so nothing is done on the server side
            auto isPrecisionModeEnabled = BehaviourUtil.togglePrecisionModeEnabled(heldItem);
            auto player = mc().player;
            auto component = Constants.translatableComponent("prsm.mm." + isPrecisionModeEnabled);
            
            //mm = mode message
            #if MC19_2 || MC19_3
                player.sendSystemMessage(component);
            #else
                player.sendMessage(component #if !MC12, Util.NIL_UUID #endif);
            #endif
            
            
            player.playSound(
                    #if MC12 SoundEvents.BLOCK_LEVER_CLICK #else SoundEvents.LEVER_CLICK #endif,
                                                                 0.4F,
                                                                 1.0F - 0.1F * (isPrecisionModeEnabled ? 1 : 2)
            );
            
            BehaviourUtil.saveToolConfigurations();
        }
    }

    private void processMouseInput()
    {
        #if MC12
        if (!mc().gameSettings.keyBindAttack.isKeyDown())
        #else
        if (!mc().options.keyAttack.isDown())
        #endif
            hasPlayerBrokenBlock = false;
    }

}
