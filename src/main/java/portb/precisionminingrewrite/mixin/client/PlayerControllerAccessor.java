/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.precisionminingrewrite.mixin.client;

#if !MC12

import manifold.rt.api.NoBootstrap;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@NoBootstrap
@net.minecraftforge.api.distmarker.OnlyIn(net.minecraftforge.api.distmarker.Dist.CLIENT)
#if MC16
@Mixin(net.minecraft.client.multiplayer.PlayerController.class)
#else
@Mixin(net.minecraft.client.multiplayer.MultiPlayerGameMode.class)
#endif
public interface PlayerControllerAccessor
{
    #if MC16
    @Accessor("destroyBlockPos")
    abstract net.minecraft.util.math.BlockPos getDestroyBlockPos();
    #else
    @Accessor("destroyBlockPos")
    abstract net.minecraft.core.BlockPos getDestroyBlockPos();
    #endif
}
#endif