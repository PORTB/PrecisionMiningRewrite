/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.precisionminingrewrite.mixin.client;

import manifold.ext.rt.api.auto;
import manifold.rt.api.NoBootstrap;

import org.apache.logging.log4j.LogManager;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import portb.precisionminingrewrite.BehaviourUtil;

#if MC16
    import net.minecraft.util.Direction;
    import net.minecraft.util.math.BlockPos;
#elif !MC12
    import net.minecraft.core.BlockPos;
    import net.minecraft.core.Direction;
#endif

#if !MC12
import static portb.precisionminingrewrite.Constants.mc;
#endif
import static portb.precisionminingrewrite.PrecisionMining.hasPlayerBrokenBlock;
import static portb.precisionminingrewrite.Constants.heldItem;

@NoBootstrap
#if MC12
    @net.minecraftforge.fml.relauncher.SideOnly(net.minecraftforge.fml.relauncher.Side.CLIENT)
    @Mixin(net.minecraft.client.multiplayer.PlayerControllerMP.class)
#else
    @net.minecraftforge.api.distmarker.OnlyIn(net.minecraftforge.api.distmarker.Dist.CLIENT)
    #if MC16
        @Mixin(net.minecraft.client.multiplayer.PlayerController.class)
    #else
        @Mixin(net.minecraft.client.multiplayer.MultiPlayerGameMode.class)
    #endif
#endif
public class PlayerControllerMixin
{
    private static final auto LOGGER = LogManager.getLogger("PlayerControllerMixin");
    
    @Inject(
            method = #if MC12 "onPlayerDamageBlock" #else "continueDestroyBlock" #endif,
            at = @At(value = "RETURN", ordinal = 0),
            cancellable = true
    )
    private void blockMiningFromDestroyDelay(CallbackInfoReturnable<Boolean> cir)
    {
        if(BehaviourUtil.isPrecisionModeEnabled(heldItem()) && hasPlayerBrokenBlock)
        {
            LOGGER.debug("blockMiningFromDestroyDelay: blocked mining");
            
            cir.setReturnValue(false);
            cir.cancel();
        }
    }
    
    @Inject(
            method = #if MC12 "onPlayerDamageBlock" #else "continueDestroyBlock" #endif,
            at = @At(
                    value = "INVOKE",
                    ordinal = 1,
                    #if MC12
                        target = "Lnet/minecraft/client/multiplayer/WorldClient;getBlockState(Lnet/minecraft/util/math/BlockPos;)Lnet/minecraft/block/state/IBlockState;"
                    #elif MC16
                        target = "Lnet/minecraft/client/world/ClientWorld;getBlockState(Lnet/minecraft/util/math/BlockPos;)Lnet/minecraft/block/BlockState;"
                    #else
                        target = "Lnet/minecraft/client/multiplayer/ClientLevel;getBlockState(Lnet/minecraft/core/BlockPos;)Lnet/minecraft/world/level/block/state/BlockState;"
                    #endif
            ),
            cancellable = true
    )
    private void blockMiningFromContinueDestroy(CallbackInfoReturnable<Boolean> cir)
    {
        if(BehaviourUtil.isPrecisionModeEnabled(heldItem()) && hasPlayerBrokenBlock)
        {
            LOGGER.debug("blockMiningFromContinueDestroy: blocked mining");
            
            cir.setReturnValue(false);
            cir.cancel();
        }
    }
    
    @Inject(
            method = #if MC12 "onPlayerDamageBlock" #else "continueDestroyBlock" #endif,
            at = @At(
                    value = "FIELD",
                    #if MC12
                        target = "Lnet/minecraft/client/multiplayer/PlayerControllerMP;isHittingBlock:Z"
                    #elif MC16
                        target = "Lnet/minecraft/client/multiplayer/PlayerController;isDestroying:Z"
                    #else
                        target = "Lnet/minecraft/client/multiplayer/MultiPlayerGameMode;isDestroying:Z"
                    #endif,
                    opcode = Opcodes.PUTFIELD
            )
    )
    private void recordBlockBreak(CallbackInfoReturnable<Boolean> cir)
    {
        hasPlayerBrokenBlock = true;
    }
    
    @Inject(
            method = #if MC12 "clickBlock" #else "startDestroyBlock" #endif,
            at = @At(
                    value = "FIELD",
                    #if MC12
                        target = "Lnet/minecraft/client/multiplayer/PlayerControllerMP;isHittingBlock:Z"
                    #elif MC16
                        target = "Lnet/minecraft/client/multiplayer/PlayerController;isDestroying:Z"
                    #else
                        target = "Lnet/minecraft/client/multiplayer/MultiPlayerGameMode;isDestroying:Z"
                    #endif,
                    opcode = org.objectweb.asm.Opcodes.GETFIELD,
                    ordinal = 1,
                    shift = At.Shift.BEFORE
            ),
            cancellable = true
    )
    private void blockStartMiningOnNewBlock(CallbackInfoReturnable<Boolean> cir)
    {
        if(BehaviourUtil.isPrecisionModeEnabled(heldItem()) && hasPlayerBrokenBlock)
        {
            LOGGER.debug("blockStartMiningOnNewBlock: blocked mining");
            
            cir.setReturnValue(false);
            cir.cancel();
        }
    }
    
    #if !MC12
    @Inject(
            method = "startDestroyBlock",
            at = @At(
                    value = "INVOKE",
                    #if MC16
                        target = "Lnet/minecraft/client/multiplayer/PlayerController;sendBlockAction(Lnet/minecraft/network/play/client/CPlayerDiggingPacket$Action;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/util/Direction;)V",
                        ordinal = 1
                    #elif MC17 || MC18
                        target = "Lnet/minecraft/client/multiplayer/MultiPlayerGameMode;sendBlockAction(Lnet/minecraft/network/protocol/game/ServerboundPlayerActionPacket$Action;Lnet/minecraft/core/BlockPos;Lnet/minecraft/core/Direction;)V",
                        ordinal = 1
                    #else
                        target = "Lnet/minecraft/client/multiplayer/ClientPacketListener;send(Lnet/minecraft/network/protocol/Packet;)V"
                    #endif,
                    shift = At.Shift.AFTER
            ),
            cancellable = true
    )
    void blockEdgeCaseInstantMine(BlockPos position, Direction direction, CallbackInfoReturnable<Boolean> cir)
    {
        auto player              = mc().player;
        auto destroyedBlockPos   = ((PlayerControllerAccessor) mc().gameMode).getDestroyBlockPos();
        auto heldItem            = mc().player.getMainHandItem();
        auto destroyedBlockState = mc().level.getBlockState(destroyedBlockPos);
        auto isDestroyProgressInfinite = Float.isInfinite(destroyedBlockState.getDestroyProgress(player,
                                                                                                 player.level,
                                                                                                 destroyedBlockPos
        ));
        auto isTargetedBlockAir = destroyedBlockState.isAir();
        
        if (!position.equals(destroyedBlockPos) && isDestroyProgressInfinite && isTargetedBlockAir &&
                    BehaviourUtil.isPrecisionModeEnabled(heldItem))
        {
            LOGGER.debug("blockEdgeCaseInstantMine: blocked mining");
            hasPlayerBrokenBlock = true;
            
            cir.setReturnValue(false);
            cir.cancel();
        }
    }
    #endif
    
    @Inject(
            #if MC12
                method = "clickBlock"
            #elif MC16 || MC17 || MC18
                method = "startDestroyBlock"
            #else
                method = "lambda$startDestroyBlock$1"
            #endif,
            at = @At(
                    value = "INVOKE",
                    #if MC12
                        target = "Lnet/minecraft/client/multiplayer/PlayerControllerMP;onPlayerDestroyBlock(Lnet/minecraft/util/math/BlockPos;)Z"
                    #elif MC16
                        target = "Lnet/minecraft/client/multiplayer/PlayerController;destroyBlock(Lnet/minecraft/util/math/BlockPos;)Z",
                        ordinal = 1
                    #elif MC17 || MC18
                        target = "Lnet/minecraft/client/multiplayer/MultiPlayerGameMode;destroyBlock(Lnet/minecraft/core/BlockPos;)Z",
                        ordinal = 1
                    #else
                        target = "Lnet/minecraft/client/multiplayer/MultiPlayerGameMode;destroyBlock(Lnet/minecraft/core/BlockPos;)Z"
                    #endif
            )
    )
    void recordInstantMine(CallbackInfoReturnable<Boolean> cir)
    {
        hasPlayerBrokenBlock = true;
    }
}
