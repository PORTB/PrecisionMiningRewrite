/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.precisionminingrewrite;

import manifold.ext.rt.api.auto;
import manifold.rt.api.NoBootstrap;

#if MC12 || MC16
import net.minecraft.item.ItemStack;
#endif

#if MC12
import net.minecraft.item.ItemShears;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
#else
import net.minecraftforge.fml.loading.FMLPaths;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

#if MC16
import net.minecraft.world.storage.FolderName;
import net.minecraft.item.ShearsItem;
#else
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.LevelResource;
import net.minecraftforge.common.ToolAction;
import net.minecraftforge.common.ToolActions;
#endif
#endif

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static portb.precisionminingrewrite.Constants.mc;

@NoBootstrap
#if MC12
@SideOnly(Side.CLIENT)
#else
@OnlyIn(Dist.CLIENT)
#endif
public class BehaviourUtil
{
    /**
     * Tracks which tools have precision mode enabled based on a hash of the item.
     * Tools that are present in the set have precision mode on,
     * Tools absent from the set have it off.
     */
    private static Set<Integer> TOOLS_PRECISION_MODE = null;
    
    public static boolean isToolConfigurationLoaded()
    {
        return TOOLS_PRECISION_MODE == null;
    }
    
    /**
     * Read tool config from file
     */
    public static void loadToolConfigurations()
    {
        auto file = getToolModeFile();
        
        //Create a set with a restricted size
        TOOLS_PRECISION_MODE = Collections.newSetFromMap(new LinkedHashMap<Integer, Boolean>(){
            /**
             * Restrict the size of the set
             */
            @Override
            protected boolean removeEldestEntry(Map.Entry eldest)
            {
                return size() > Constants.MAX_CLIENT_TRACKED_ITEMS;
            }
        });
        
        if(Files.exists(file))
        {
            try
            {
                //read the file as a buffer of ints
                IntBuffer intBuffer = ByteBuffer.wrap(Files.readAllBytes(file))
                                      .order(ByteOrder.BIG_ENDIAN)
                                      .asIntBuffer();
                
                while(intBuffer.hasRemaining())
                    TOOLS_PRECISION_MODE.add(intBuffer.get());
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        }
    }
    
    /**
     * Save tool configs to file.
     */
    public static void saveToolConfigurations()
    {
        try
        {
            auto file = getToolModeFile();
            //write to the file as a buffer of ints
            auto byteBuf = ByteBuffer.allocate(TOOLS_PRECISION_MODE.size() * 4) //1 int = 4 bytes
                                     .order(ByteOrder.BIG_ENDIAN);
            auto intBuf = byteBuf.asIntBuffer();
            
            for (auto hash : TOOLS_PRECISION_MODE)
                intBuf.put(hash);
            
            Files.createDirectories(file.getParent());
            Files.write(file, byteBuf.array());
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
    
    private static Path getToolModeFile()
    {
        #if MC12
        if(mc().getIntegratedServer() != null)
            return mc().mcDataDir
                           .toPath()
                           .resolve("saves")
                           .resolve(mc().getIntegratedServer().getFolderName())
                           .resolve("precision_mode_tools.prsm");
        else
            return Loader.instance().getConfigDir().toPath().resolve("precisionmining").resolve(
                    mc().getCurrentServerData().serverIP.replaceAll("[\\/:]", "_") + ".prsm");
        
        #else
        if(mc().isLocalServer())
            return mc().getSingleplayerServer().getWorldPath(#if MC16 FolderName.ROOT #else LevelResource.ROOT #endif).resolve("precision_mode_tools.prsm");
        else
            return FMLPaths.CONFIGDIR.get().resolve("precisionmining").resolve(mc().getCurrentServer().ip + ".prsm");
        #endif
    }
    
    public static boolean isPrecisionModeEnabled(ItemStack itemStack)
    {
        return TOOLS_PRECISION_MODE.contains(hashItem(itemStack));
    }
    
    /**
     * Toggles precision mode for an item
     * @return Whether the item now has precision mode enabled
     */
    public static boolean togglePrecisionModeEnabled(ItemStack itemStack)
    {
        auto hash = hashItem(itemStack);
  
        if(TOOLS_PRECISION_MODE.contains(hash))
        {
            TOOLS_PRECISION_MODE.remove(hash);
            return false;
        }
        else
        {
            TOOLS_PRECISION_MODE.add(hash);
            return true;
        }
    }
    
    private static int hashItem(ItemStack itemStack)
    {
        #if MC12
        return Objects.hash(
                itemStack.getItem().getRegistryName().toString(),
                itemStack.getDisplayName(),
                itemStack.getEnchantmentTagList(),
                itemStack.getRepairCost()
        );
        #else
        return Objects.hash(
                #if MC19_2
                    net.minecraft.core.Registry.ITEM.getKey(itemStack.getItem()).toString()
                #elif MC19_3
                    net.minecraft.core.registries.BuiltInRegistries.ITEM.getKey(itemStack.getItem()).toString()
                #else
                    itemStack.getItem().getRegistryName().toString()
                #endif,
                itemStack.getHoverName().getString(),
                itemStack.getEnchantmentTags(),
                itemStack.getBaseRepairCost()
        );
        #endif
    }
    
    static public boolean isItemTool(ItemStack stack)
    {
        #if MC12
        return stack.getItem() instanceof ItemShears || !stack.getItem().getToolClasses(stack).isEmpty();
        #elif MC16
        return stack.getItem() instanceof ShearsItem || !stack.getToolTypes().isEmpty();
        #else
        var toolActions = new ToolAction[]{ToolActions.AXE_DIG, ToolActions.HOE_DIG, ToolActions.PICKAXE_DIG, ToolActions.SHEARS_DIG, ToolActions.SHOVEL_DIG};
        
        for (ToolAction toolAction : toolActions)
        {
            if(stack.getItem().canPerformAction(stack, toolAction))
                return true;
        }
        
        return false;
        #endif
    }
}
