/*
 * Copyright (c) PORTB 2023
 *
 * Licensed under GNU LGPL v3
 * https://www.gnu.org/licenses/lgpl-3.0.txt
 */

package portb.precisionminingrewrite;

import manifold.ext.rt.api.auto;
import manifold.rt.api.NoBootstrap;
import net.minecraft.client.Minecraft;

#if !MC12
import org.lwjgl.glfw.GLFW;
#endif

#if MC12 || MC16
    import net.minecraft.client.settings.KeyBinding;
#else
    import net.minecraft.client.KeyMapping;
#endif

@NoBootstrap
public class Constants
{
    public static auto TOGGLE_PRECISION_MODE_KEYBIND = new #if MC12 || MC16 KeyBinding #else KeyMapping #endif(
            "prsm.key.toggle",
            #if MC12 43 #else GLFW.GLFW_KEY_BACKSLASH #endif,
            "prsm.cat"
    );
    
    public static final int MAX_CLIENT_TRACKED_ITEMS = 1048576 / 4; //Limit file size to 1MB. Each tool entry takes up 4 bytes (java int is int32)
    
    public static auto translatableComponent(String key)
    {
        #if MC12
            return new net.minecraft.util.text.TextComponentTranslation(key);
        #elif MC16
            return new net.minecraft.util.text.TranslationTextComponent(key);
        #elif MC17 || MC18
            return new net.minecraft.network.chat.TranslatableComponent(key);
        #elif MC19_2 || MC19_3
            return net.minecraft.network.chat.Component.translatable(key);
        #endif
    }
    
    public static Minecraft mc()
    {
        #if MC12
            return Minecraft.getMinecraft();
        #else
            return Minecraft.getInstance();
        #endif
    }
    
    public static auto heldItem()
    {
        #if MC12
            return mc().player.getHeldItemMainhand();
        #else
            return mc().player.getMainHandItem();
        #endif
    }
}
